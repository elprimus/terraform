provider "aws" {
    region     = "sa-east-1"
}
resource "aws_key_pair" "auth" {
  key_name   = "aws_terraform"
  public_key = "${file("/home/tadeu/.ssh/aws_terraform.pub")}"
}
resource "aws_vpc" "rede_ec2" {
    cidr_block = "10.0.0.0/16"
}
data "aws_availability_zones" "available" {}
resource "aws_subnet" "rede_apache" {
    availability_zone       = "${data.aws_availability_zones.available.names[0]}"
    vpc_id                  = "${aws_vpc.rede_ec2.id}"
    cidr_block              = "10.0.1.0/24"
    map_public_ip_on_launch = true
}
resource "aws_subnet" "rede_nginx" {
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    vpc_id                  = "${aws_vpc.rede_ec2.id}"
    cidr_block              = "10.0.3.0/24"
    map_public_ip_on_launch = true
}
resource "aws_internet_gateway" "rede_ec2" {
    vpc_id = "${aws_vpc.rede_ec2.id}"
}
resource "aws_route" "internet" {
    route_table_id          = "${aws_vpc.rede_ec2.main_route_table_id}"
    destination_cidr_block  = "0.0.0.0/0"
    gateway_id              =   "${aws_internet_gateway.rede_ec2.id}"
}
resource "aws_security_group" "group_elb" {
    name            = "group_elb"
    description     = "Libera trafeco na porta 80"
    vpc_id          = "${aws_vpc.rede_ec2.id}"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}
resource "aws_security_group" "group_ec2" {
    name            = "group_ec2"
    description     = "Libera trafeco na porta 80 e 22"
    vpc_id          = "${aws_vpc.rede_ec2.id}"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}
resource "aws_lb" "loadBalance" {
    name                = "loadbalance"
    internal            = false
    load_balancer_type  = "application"
    security_groups     = ["${aws_security_group.group_elb.id}"]
    subnets             = ["${aws_subnet.rede_apache.id}", "${aws_subnet.rede_nginx.id}"]
}
resource "aws_lb_target_group" "webGroupTarget" {
    name     = "webGroupTarget"
    port     = 80
    protocol = "HTTP"
    vpc_id   = "${aws_vpc.rede_ec2.id}"
}
resource "aws_lb_target_group_attachment" "webGroupTarget1" {
    target_group_arn    = "${aws_lb_target_group.webGroupTarget.arn}"
    target_id           = "${aws_instance.srv_apache.id}"
    port                = 80
}
resource "aws_lb_target_group_attachment" "webGroupTarget2" {
    target_group_arn    = "${aws_lb_target_group.webGroupTarget.arn}"
    target_id           = "${aws_instance.srv_nginx.id}"
    port                = 80
}

resource "aws_lb_listener" "loadBalance" {
  load_balancer_arn = "${aws_lb.loadBalance.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.webGroupTarget.arn}"
  }
}
resource "aws_instance" "srv_apache" {
    ami                     = "ami-0a79841e0fda419d6"
    instance_type           = "t2.micro"
    vpc_security_group_ids  = ["${aws_security_group.group_ec2.id}"]
    subnet_id               = "${aws_subnet.rede_apache.id}"
    key_name                = "${aws_key_pair.auth.id}"

    provisioner "remote-exec"{
        inline = [
            "sudo apt-get -y update",
            "sudo apt-get -y install apache2",
            "sudo service apache2 start",
        ]

        connection {
            type = "ssh"
            user = "admin"
            private_key = "${file("/home/tadeu/.ssh/aws_terraform")}"
        }
    }
    tags{
        Name = "Apache"
    }  
}
resource "aws_instance" "srv_nginx" {
    ami                     = "ami-0a79841e0fda419d6"
    instance_type           = "t2.micro"
    vpc_security_group_ids  = ["${aws_security_group.group_ec2.id}"]
    subnet_id               = "${aws_subnet.rede_nginx.id}"
    key_name = "${aws_key_pair.auth.id}"
    provisioner "remote-exec"{
        inline = [
            "sudo apt-get -y update",
            "sudo apt-get -y install nginx",
            "sudo service nginx start",
        ]

        connection {
            type = "ssh"
            user = "admin"
            private_key = "${file("/home/tadeu/.ssh/aws_terraform")}"
        }
    }
    tags{
        Name = "Nginx"
    }  
}