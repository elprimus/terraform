

variable "rede_ec2" {
  description = "VPC ID"
}

variable "srv_apache" {
    description  = "Instancia com apache"
}

variable "srv_nginx" {
    description  = "Instancia com ngix"
}
