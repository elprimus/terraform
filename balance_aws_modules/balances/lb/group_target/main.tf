resource "aws_lb_target_group" "webGroupTarget" {
    name     = "webGroupTarget"
    port     = 80
    protocol = "HTTP"
    vpc_id   = "${var.rede_ec2}"
}
resource "aws_lb_target_group_attachment" "webGroupTarget1" {
    target_group_arn    = "${aws_lb_target_group.webGroupTarget.arn}"
    target_id           = "${var.srv_apache}"
    port                = 80
}
resource "aws_lb_target_group_attachment" "webGroupTarget2" {
    target_group_arn    = "${aws_lb_target_group.webGroupTarget.arn}"
    target_id           = "${var.srv_nginx}"
    port                = 80
}