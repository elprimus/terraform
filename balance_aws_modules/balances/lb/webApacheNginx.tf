resource "aws_lb" "webApacheNginx" {
    name                = "webApacheNginx"
    internal            = false
    load_balancer_type  = "application"
    security_groups     = ["${var.group_bl}"]
    subnets             = ["${var.rede_apache}", "${var.rede_nginx}"]
}