output "group_lb_id" {
  value = "${aws_security_group.group_lb.id}"
}

output "group_ec2_id" {
  value = "${aws_security_group.group_ec2.id}"
}