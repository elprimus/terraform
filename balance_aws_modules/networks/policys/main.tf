resource "aws_security_group" "group_lb" {
    name            = "group_lb"
    description     = "Libera trafeco na porta 80"
    vpc_id          = "${var.rede_ec2}"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}
resource "aws_security_group" "group_ec2" {
    name            = "group_ec2"
    description     = "Libera trafeco na porta 80 e 22"
    vpc_id          = "${var.rede_ec2}"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}