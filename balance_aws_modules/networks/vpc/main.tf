resource "aws_vpc" "rede_ec2" {
    cidr_block = "${var.vpc_network}"
}

resource "aws_internet_gateway" "rede_ec2" {
    vpc_id = "${aws_vpc.rede_ec2.id}"
}
resource "aws_route" "internet" {
    route_table_id          = "${aws_vpc.rede_ec2.main_route_table_id}"
    destination_cidr_block  = "0.0.0.0/0"
    gateway_id              =   "${aws_internet_gateway.rede_ec2.id}"
}


