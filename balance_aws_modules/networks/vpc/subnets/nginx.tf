resource "aws_subnet" "rede_nginx" {
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    vpc_id                  = "${var.rede_ec2}"
    cidr_block              = "${var.subnet_nginx}"
    map_public_ip_on_launch = true
}