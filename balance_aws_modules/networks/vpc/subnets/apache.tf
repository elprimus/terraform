resource "aws_subnet" "rede_apache" {
    availability_zone       = "${data.aws_availability_zones.available.names[0]}"
    vpc_id                  = "${var.rede_ec2}"
    cidr_block              = "${var.subnet_apache}"
    map_public_ip_on_launch = true
}