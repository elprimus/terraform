output "rede_apache" {
  value = "${aws_subnet.rede_apache.id}"
}

output "rede_nginx" {
  value = "${aws_subnet.rede_nginx.id}"
}