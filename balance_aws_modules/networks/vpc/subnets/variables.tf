variable "rede_ec2" {
  description = "VPC ID"
}

variable "subnet_apache" {
    description = "Rede para instancias Apache"
    default     = "10.0.1.0/24"
}

variable "subnet_nginx" {
    description = "Rede para instancias Nginx"
    default     = "10.0.3.0/24"
}