output "srv_nginx_id" {
  value = "${aws_instance.srv_nginx.id}"
}

output "srv_apache_id" {
  value = "${aws_instance.srv_apache.id}"
}

