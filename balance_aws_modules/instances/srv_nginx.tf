resource "aws_instance" "srv_nginx" {
    ami                     = "${var.ami}"
    instance_type           = "${var.instance_type}"
    vpc_security_group_ids  = ["${var.vpc_groups_ec2_id}"]
    subnet_id               = "${var.rede_nginx_id}"
    key_name                = "${var.aws_key_pair_auth}"
    provisioner "remote-exec"{
        inline = [
            "sudo apt-get -y update",
            "sudo apt-get -y install nginx",
            "sudo service nginx start",
        ]
        connection {
            type = "${var.connection_type}"
            user = "${var.user_SO}"
            private_key = "${file(var.private_key)}"
        }
    }
    tags{
        Name = "Nginx"
    }  
}