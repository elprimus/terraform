resource "aws_instance" "srv_apache" {
    ami                     = "${var.ami}"
    instance_type           = "${var.instance_type}"
    vpc_security_group_ids  = ["${var.vpc_groups_ec2_id}"]
    subnet_id               = "${var.rede_apache_id}"
    key_name                = "${var.aws_key_pair_auth}"

    provisioner "remote-exec"{
        inline = [
            "sudo apt-get -y update",
            "sudo apt-get -y install apache2",
            "sudo service apache2 start",
        ]

        connection {
            type = "${var.connection_type}"
            user = "${var.user_SO}"
            private_key = "${file(var.private_key)}"
        }
    }
    tags{
        Name = "Apache"
    }  
}