variable "ami" {
    description = "Imagem do S.O."
    default     =  "ami-0a79841e0fda419d6"
}

variable "instance_type" {
    description = "Modelo da Instancia"
    default     = "t2.micro"
}

variable "connection_type" {
    default = "ssh"
}

variable "user_SO" {
    default = "admin"
}



variable "aws_key_pair_auth" {
}

variable "private_key" {
    default = "/home/tadeu/.ssh/aws_terraform"
}

variable "vpc_groups_ec2_id" {}
variable "rede_apache_id" {}
variable "rede_nginx_id" {}

