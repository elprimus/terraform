provider "aws" {
    region     = "sa-east-1"
}
resource "aws_key_pair" "auth" {
  key_name   = "aws_terraform"
  public_key = "${file("/home/tadeu/.ssh/aws_terraform.pub")}"
}
module "vpc" {
    source = "networks/vpc/"
}

module "policys" {
    source      = "networks/policys/"
    rede_ec2    = "${module.vpc.rede_ec2_id}"
}

module "subnets" {
    source      = "networks/vpc/subnets/"
    rede_ec2    = "${module.vpc.rede_ec2_id}"
}

module "alb" {
    source          = "balances/lb/"
    group_bl        = "${module.policys.group_lb_id}"
    rede_apache     = "${module.subnets.rede_apache}"
    rede_nginx      = "${module.subnets.rede_nginx}"
}

module "groupTarget" {
    source          = "balances/lb/group_target/"
    rede_ec2        = "${module.vpc.rede_ec2_id}"
    srv_apache      = "${module.instances.srv_apache_id}"
    srv_nginx       = "${module.instances.srv_nginx_id}"
}

module "listerner" {
    source              = "balances/lb/listener/"
    lb_arn              = "${module.alb.lb_arn}"
    webGroupTarget_arn  = "${module.groupTarget.webGroupTarget_arn}"
}

module "instances" {
    source              = "instances/"
    vpc_groups_ec2_id   = "${module.policys.group_ec2_id}"
    rede_apache_id      = "${module.subnets.rede_apache}"
    rede_nginx_id       = "${module.subnets.rede_nginx}"
    aws_key_pair_auth   = "${aws_key_pair.auth.id}"
  
}
